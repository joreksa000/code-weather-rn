import {NavigationContainer, NavigationProp} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import WeatherScreen from '../../presentasion/screens/Main/WeatherScreen';
import Splash from '../../presentasion/screens/SplashScreen/Splash';
import {RootStackParamList} from '../../domain/entities/routeEntities';
import SearchScreen from '../../presentasion/screens/Search/SearchScreen';

const Stack = createNativeStackNavigator();
export type StackNavigation = NavigationProp<RootStackParamList>;

const Route: React.FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, animation: 'slide_from_right'}}
        initialRouteName="splash">
        <Stack.Screen name="splash" component={Splash} />
        <Stack.Screen name="Home" component={WeatherScreen} />
        <Stack.Screen name="Details" component={SearchScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
