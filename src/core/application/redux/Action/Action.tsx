import axios from 'axios';
import {
  API_KEY,
  API_URLS_CURRENT,
  API_URLS_HOURLY,
} from '../../../infrastructure/constant';
import {LocationPermissions} from '../../Utils/LocationPermissions';
import Geolocation from '@react-native-community/geolocation';
import {Location} from '../../../domain/entities/weatherEnitites';

export const Removes = () => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: 'LOADING',
      });
      setTimeout(() => {
        dispatch({
          type: 'REMOVE',
        });
      }, 2000);
    } catch (e) {
      console.log(e, 'err remove');
    }
  };
};
export const GetLocationName = (Location: string) => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: 'LOADING',
      });
      await axios
        .get(`${API_URLS_CURRENT}q=${Location}&appid=${API_KEY}`)
        .then(response => {
          dispatch({
            type: 'GET_DATA_LOCATION_NAME',
            data: response,
          });
        })
        .catch(error => {
          console.error('Error fetching data:', error);
        });
    } catch (e) {
      console.log(e, 'err');
    }
  };
};

export const GetCurrentData = () => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: 'LOADING',
      });
      const result = await LocationPermissions();
      if (result) {
        const position = await new Promise<Location | null>(
          (resolve, reject) => {
            Geolocation.getCurrentPosition(
              (position: any) => resolve(position),
              error => reject(error),
              {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
            );
          },
        );
        if (position) {
          await axios
            .get(
              `${API_URLS_CURRENT}lat=${position.coords.latitude}&lon=${position.coords.longitude}&appid=${API_KEY}`,
            )
            .then((res: any) => {
              const data = res.data;
              dispatch({
                type: 'GET_DATA_CURRENT',
                data: data,
              });
              dispatch(
                GetHourlyData(
                  position.coords.latitude,
                  position.coords.longitude,
                ),
              );
            });
        }
      }
    } catch (e) {
      console.log(e, 'err current data');
    }
  };
};

export const GetHourlyData = (lat: any, long: any) => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: 'LOADING',
      });
      await axios
        .get(`${API_URLS_HOURLY}lat=${lat}&lon=${long}&appid=${API_KEY}`)
        .then((res: any) => {
          const data = res.data;

          dispatch({
            type: 'GET_DATA_HOURLY',
            data: data,
          });
        });
    } catch (e) {
      console.log(e, 'err Hourly data');
    }
  };
};

export const GetSearchText = (city: any) => {
  return async (dispatch: any) => {
    try {
      dispatch({
        type: 'LOADING',
      });

      await axios
        .get(`${API_URLS_CURRENT}q=${city}&appid=${API_KEY}`)
        .then((res: any) => {
          const data = res.data;
          dispatch({
            type: 'GET_DATA_SEARCH',
            data: data,
          });
          dispatch(GetHourlyData(data.coord.lat, data.coord.lon));
        });
    } catch (e) {
      console.log(e, 'err search data');
    }
  };
};
