import thunk from 'redux-thunk';
import {applyMiddleware, legacy_createStore as createStore} from 'redux';
import rootReducer from './reducer';

const StoreRedux = createStore(rootReducer, applyMiddleware(thunk));

export type IRootState = ReturnType<typeof StoreRedux.getState>;
export type AppDispatch = typeof StoreRedux.dispatch;
export default StoreRedux;
