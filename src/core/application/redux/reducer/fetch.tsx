export interface Reduces {
  Data: null;
  CURRENT_DATA: null;
  HOURLY_DATA: null;
  SEARCH_DATA: null;
  dataParams: null;
  isLoading: boolean;
}

const Initial = {
  Data: null,
  CURRENT_DATA: null,
  HOURLY_DATA: null,
  isLoading: false,
  SEARCH_DATA: null,
  dataParams: null,
};

const Fetch = (state: Reduces = Initial, action: any): Reduces => {
  switch (action.type) {
    case 'LOADING':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_DATA_LOCATION_NAME':
      return {
        ...state,
        isLoading: false,
        Data: action.data,
      };
    case 'GET_DATA_CURRENT':
      return {
        ...state,
        isLoading: false,
        CURRENT_DATA: action.data,
        dataParams: action.data,
      };
    case 'GET_DATA_HOURLY':
      return {
        ...state,
        isLoading: false,
        HOURLY_DATA: action.data,
      };
    case 'GET_DATA_SEARCH':
      return {
        ...state,
        SEARCH_DATA: action.data,
        CURRENT_DATA: action.data,
      };
    case 'REMOVE':
      return {
        ...state,
        isLoading: false,
        SEARCH_DATA: null,
      };
    default:
      return state;
  }
};

export default Fetch;
