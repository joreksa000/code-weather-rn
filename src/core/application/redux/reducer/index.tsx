import {combineReducers} from '@reduxjs/toolkit';
import fetch from './fetch';

const rootReducer = combineReducers({
  fetch,
});

export default rootReducer;
