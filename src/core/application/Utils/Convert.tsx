export const ConvertCel = (tempInKelvin: number) => {
  const tempInCelsius = Math.round(tempInKelvin - 273.15);
  return tempInCelsius;
};

export const TimeConvert = (times: string) => {
  const timeParts = times.split(' ')[1].split(':');
  const time = `${timeParts[0]}:${timeParts[1]}`;
  return time;
};

export const FormatedConvert = (day: string) => {
  const dateTimeParts = day.split(' ');
  const timePart = dateTimeParts[1];
  const currentDate = new Date(day);
  let displayText = timePart;

  if (timePart === '00:00:00') {
    displayText = currentDate.toLocaleDateString(undefined, {
      month: 'short',
      day: 'numeric',
    });
  }
  return displayText;
};

export const FormatedConvert2 = (day: string) => {
  const dateTimeParts = day.split(' ');
  const timePart = dateTimeParts[1];
  const currentDate = new Date(day);
  let displayText = timePart;

  if (timePart === '00:00:00') {
    displayText = currentDate
      .toLocaleDateString(undefined, {
        weekday: 'short',
        month: 'short',
        day: 'numeric',
      })
      .replace(/,/g, '');
  }
  return displayText;
};
