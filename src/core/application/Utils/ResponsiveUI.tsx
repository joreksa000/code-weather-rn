import {Dimensions} from 'react-native';

export const windowWidth = Dimensions.get('window').width;
export const windowHeight = Dimensions.get('window').height;

let widthDefault = 428;
let heightDefault = 926;

export const responsiveWidth = (width: number) => {
  return (windowWidth * width) / widthDefault;
};

export const responsiveHeight = (height: number) => {
  return (windowHeight * height) / heightDefault;
};
