export interface Location {
  coords: any;
  longitude: any;
  latitude: any;
}

export interface WeatherScreenProps {
  loading: boolean;
}
