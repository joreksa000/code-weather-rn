export interface TypesWinds {
  data: null;
}

export interface Types {
  data: {
    wind: {speed: number; deg: number};
    clouds: {
      all: number;
    };
    main: {
      humidity: number;
      pressure: number;
      temp_min: number;
    };
    visibility: number;
  };
}
