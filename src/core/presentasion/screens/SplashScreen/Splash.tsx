import {Image, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {GetCurrentData} from '../../../application/redux/Action/Action';
import {connect, useDispatch} from 'react-redux';
import {AppDispatch} from '../../../application/redux/Store';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../../../domain/entities/routeEntities';
import {colors} from '../../Assets/Colors/Colors';
import OpenweatherLogo from '../../Assets/Image/OpenWeather.png';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
const Splash = (props: any) => {
  const dispatch = useDispatch<AppDispatch>();

  const navigation = useNavigation<RootStackParamList>();

  const GetCurrents = async () => {
    try {
      dispatch(GetCurrentData());
    } catch (error) {
      console.error('Error getting current data:', error);
    }
  };
  useEffect(() => {
    GetCurrents().then(() => {
      setTimeout(() => {
        navigation.navigate('Home');
      }, 3000);
    });
  }, []);
  return (
    <View style={style.container}>
      <View style={style.conLogo}>
        <Image source={OpenweatherLogo} style={style.ImageContainer} />
        <Text style={style.textStyles}>Fetching Weather ...</Text>
      </View>
      <View style={{marginBottom: responsiveHeight(10)}}>
        <Text style={style.textStyles}>Open Weather Clone</Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  ImageContainer: {
    height: responsiveHeight(200),
    width: responsiveHeight(200),
    resizeMode: 'contain',
  },
  conLogo: {justifyContent: 'center', flex: 1, alignItems: 'center'},
  textStyles: {
    color: colors.white,
    fontWeight: '300',
    fontSize: responsiveHeight(18),
  },
});

const mapStateToProps = (state: any) => ({
  data: state.fetch.CURRENT_DATA,
  loading: state.fetch.isLoading,
});

export default connect(mapStateToProps)(Splash);
