import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import SearchIcons from 'react-native-vector-icons/AntDesign';
import X from 'react-native-vector-icons/Feather';

import LocationIcons from 'react-native-vector-icons/Octicons';
import {ConvertCel} from '../../../application/Utils/Convert';
import {useNavigation, useRoute} from '@react-navigation/native';
import {RootStackParamList} from '../../../domain/entities/routeEntities';
import {connect, useDispatch, useSelector} from 'react-redux';
import {AppDispatch, IRootState} from '../../../application/redux/Store';
import {GetSearchText} from '../../../application/redux/Action/Action';
import Loading from '../../Component/Loading/Loading';

interface Params {
  loading: any;
}
interface WeatherData {
  name: string;
  sys: {
    country: string;
  };
  main: {
    temp: number;
  };
  weather: {
    icon: string;
  }[];
}

const SearchScreen: React.FC<Params> = props => {
  const [searchQueary, setSearchQueary] = useState('');
  const route = useRoute<any>();
  const {data} = route.params;

  const dispatch = useDispatch<AppDispatch>();

  const navigation = useNavigation<RootStackParamList>();

  const dataBaru = useSelector(
    (state: IRootState) => state?.fetch?.SEARCH_DATA,
  ) as WeatherData | null;

  const data_currents = useSelector(
    (state: IRootState) => state?.fetch?.CURRENT_DATA,
  ) as WeatherData | null;
  const hasils = dataBaru?.name !== data_currents?.name ? true : false;

  let configImg: string | undefined;

  if (dataBaru == null) {
    configImg = data?.weather[0]?.icon;
  } else if (hasils) {
    configImg = data?.weather[0]?.icon;
  } else {
    configImg = dataBaru?.weather[0]?.icon;
  }

  const ImageData = `https://openweathermap.org/img/wn/${configImg}@2x.png`;

  let renderContent;

  const Handles = () => {
    if (hasils === false) {
      navigation.navigate('Home');
    } else {
      navigation.navigate('splash');
    }
  };
  if (props.loading) {
    renderContent = <View style={style.containerIsi}></View>;
  } else if (dataBaru === null && hasils !== true) {
    renderContent = (
      <View style={style.containerIsi}>
        <View style={style.containerIsi2}>
          <LocationIcons size={15} name="location" color={colors.white} />
          <Text
            style={[style.textStyle, {marginHorizontal: responsiveHeight(10)}]}>
            {data.name}, {data.sys.country}
          </Text>
        </View>
        <View style={style.containerIsi2}>
          <Text style={style.textStyle}>{ConvertCel(data.main.temp)}°C</Text>
          <Image style={style.imageCon} source={{uri: ImageData}} />
        </View>
      </View>
    );
  } else {
    renderContent = (
      <TouchableOpacity style={style.containerIsi} onPress={Handles}>
        <View style={style.containerIsi2}>
          <LocationIcons size={15} name="location" color={colors.white} />
          <Text
            style={[style.textStyle, {marginHorizontal: responsiveHeight(10)}]}>
            {hasils ? data.name : dataBaru?.name},{' '}
            {hasils ? data.sys?.country : dataBaru?.sys?.country}
          </Text>
        </View>
        <View style={style.containerIsi2}>
          <Text style={style.textStyle}>
            {ConvertCel(
              Number(hasils ? data?.main?.temp : dataBaru?.main?.temp),
            )}
            °C
          </Text>
          <Image style={style.imageCon} source={{uri: ImageData}} />
        </View>
      </TouchableOpacity>
    );
  }
  const handleSearch = (text: string) => {
    setSearchQueary(text);
  };

  const handleAPISeach = () => {
    dispatch(GetSearchText(searchQueary));
  };

  return (
    <View style={style.container}>
      <View style={style.containerInput}>
        <View style={style.containerRow}>
          <SearchIcons size={20} name="search1" color={'#929294'} />
          <TextInput
            style={style.inTextstyle}
            placeholder="Search..."
            value={searchQueary}
            onChangeText={handleSearch}
            autoCapitalize="characters"
            placeholderTextColor={'#929294'}
            onSubmitEditing={() => {
              handleAPISeach();
            }}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}>
          <X size={25} name="x" color={colors.white} />
        </TouchableOpacity>
      </View>
      {renderContent}
      <Loading isLoading={props.loading} />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.black,
    flex: 1,
  },
  textStyle: {
    color: colors.white,
    fontSize: responsiveHeight(18),
    fontWeight: '300',
  },
  containerInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: responsiveHeight(80),
    padding: responsiveHeight(15),
    alignItems: 'center',
    elevation: 5,
  },
  containerRow: {
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#1c1c1e',
    borderRadius: 8,
    paddingHorizontal: responsiveHeight(10),
    width: '90%',
  },
  inTextstyle: {
    color: colors.white,
    alignItems: 'center',
    padding: responsiveHeight(10),
    width: '100%',
  },
  imageCon: {
    height: responsiveHeight(60),
    width: responsiveHeight(60),
    resizeMode: 'contain',
  },
  containerIsi: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: colors.white,
    paddingHorizontal: responsiveHeight(15),
  },
  containerIsi2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
const mapStateToProps = (state: any) => ({
  loading: state.fetch.isLoading,
});

export default connect(mapStateToProps)(SearchScreen);
