import {View, Text, StyleSheet} from 'react-native';
import React, {useState} from 'react';
import Loading from '../../Component/Loading/Loading';
import {connect, useDispatch, useSelector} from 'react-redux';
import {AppDispatch, IRootState} from '../../../application/redux/Store';
import {WeatherScreenProps} from '../../../domain/entities/weatherEnitites';
import Header from '../../Component/Header/Header';
import {colors} from '../../Assets/Colors/Colors';
import MidCards from '../../Component/Card/MidCards';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import MainComponent from '../../Component/MainComponent/MainComponent';
import WeatherList from '../../Component/List/WeatherList';
import {ScrollView} from 'react-native-virtualized-view';
import {useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../../../domain/entities/routeEntities';
import {Removes} from '../../../application/redux/Action/Action';

interface WeatherData {
  coord: {
    lat: number;
    lon: number;
  };
}
const WeatherScreen: React.FC<WeatherScreenProps> = props => {
  const dispatch = useDispatch<AppDispatch>();

  const data_current = useSelector(
    (state: IRootState) => state.fetch.CURRENT_DATA,
  );

  const data_Hourly = useSelector(
    (state: IRootState) => state.fetch.HOURLY_DATA,
  );

  const data_Live = useSelector(
    (state: IRootState) => state.fetch.dataParams,
  ) as WeatherData | number | null;
  const navigation = useNavigation<RootStackParamList>();

  const SearchHandle = async () => {
    try {
      dispatch(Removes());
    } catch (e) {
      console.log(e, 'errr');
    }
  };

  const navs = () => {
    navigation.navigate('Details', {data: data_Live});
  };

  return (
    <View style={style.container}>
      {props.loading ? (
        <Loading isLoading={true} />
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <Header
            data={data_current}
            onpress={() => {
              SearchHandle().then(() => navs());
            }}
          />
          <View style={style.containerMain}>
            <View style={style.containMainCom}>
              <MainComponent data={data_current} />
            </View>
            <View>
              <Text style={style.textStyle}>
                No precipitation within an hour
              </Text>
              <MidCards data={data_current} />
            </View>
            <View style={style.ContinaerList}>
              <WeatherList data={data_Hourly} ScrollHorizontal={true} />
            </View>
            <View style={style.ContinaerList}>
              <WeatherList data={data_Hourly} ScrollHorizontal={false} />
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  containerMain: {
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  textStyle: {
    color: colors.white,
    textAlign: 'center',
    marginVertical: responsiveHeight(10),
    fontSize: responsiveHeight(16),
    fontWeight: 'bold',
  },
  containMainCom: {
    marginBottom: responsiveHeight(50),
  },
  renderContainer: {
    height: '100%',
    backgroundColor: 'red',
  },
  ContinaerList: {
    marginTop: responsiveHeight(10),
  },
});

const mapStateToProps = (state: any) => ({
  data: state.fetch.CURRENT_DATA,
  loading: state.fetch.isLoading,
});

export default connect(mapStateToProps)(WeatherScreen);
