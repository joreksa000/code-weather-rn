import {View, Text, StyleSheet} from 'react-native';
import React, {useEffect, useState} from 'react';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import ArrowLoc from 'react-native-vector-icons/FontAwesome6';
import {Types} from '../../../domain/entities/cardsEntities';
import {ConvertCel} from '../../../application/Utils/Convert';

interface Roots {
  data: null;
}
const MidCards: React.FC<Roots> = (data: any) => {
  const [mapping, setMapping] = useState<Types | null>(null);
  const [degree, setDegree] = useState('180');
  useEffect(() => {
    setMapping(data);
    setDegree(mapping !== null ? `${mapping.data.wind.deg}` : '180');
  }, [mapping]);

  const distanceInMeters = mapping?.data?.visibility;
  const distanceInKilometers =
    distanceInMeters !== undefined ? distanceInMeters / 1000 : 8000 / 1000;

  return (
    <View style={style.container}>
      <View style={style.containerText}>
        <View style={style.containerText}>
          <Text style={style.textStyle}>
            Wind: {`${mapping?.data?.wind?.speed}`}m/s
          </Text>
          <ArrowLoc
            name="location-arrow"
            size={12}
            color={'#9f9fa0'}
            style={{transform: [{rotateY: `${degree}deg`}]}}
          />
        </View>
        <Text style={style.textStyle}>
          Humidity: {`${mapping?.data?.main.humidity}`}%
        </Text>
        <Text style={style.textStyle}>UV index: 0.0</Text>
        <Text style={style.textStyle}>
          Pressure: {`${mapping?.data?.main.pressure}`}hPa
        </Text>
      </View>
      <View style={style.containerText}>
        <Text style={style.textStyle}>
          Visibility: {`${distanceInKilometers}`}.0km
        </Text>
        <Text style={style.textStyle}>
          Dew point: {ConvertCel(Number(mapping?.data?.main?.temp_min))}°C
        </Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.gray,
    borderRadius: 10,
    marginHorizontal: responsiveHeight(10),
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: responsiveHeight(80),
    padding: responsiveHeight(15),
  },
  textStyle: {
    color: colors.white,
    fontSize: responsiveHeight(16),
    fontWeight: '500',
  },
  containerText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default MidCards;
