import React, {useState} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Image,
  TouchableHighlight,
} from 'react-native';
import {
  ConvertCel,
  FormatedConvert,
  FormatedConvert2,
  TimeConvert,
} from '../../../application/Utils/Convert';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import ChevronIcon from 'react-native-vector-icons/Feather';
import Modals from '../Modals/Modals';

interface Props {
  ScrollHorizontal: boolean;
  data: any;
}

const WeatherList: React.FC<Props> = ({data, ScrollHorizontal}) => {
  const [changes, setChanges] = useState(false);
  const [datas, setDatas] = useState();
  const filteredData = data?.list?.filter((item: any) => {
    const timePart = item.dt_txt.split(' ')[1];
    return timePart === '00:00:00';
  });
  const dataChange = ScrollHorizontal ? data?.list : filteredData;
  const renderWeatherItem = ({item, index}: any) => {
    const images = item.weather[0].icon;
    const imageIcons = `https://openweathermap.org/img/wn/${images}@2x.png`;

    const dateTimeParts = item?.dt_txt?.split(' ');
    const timePart = dateTimeParts[1];

    const border = ScrollHorizontal === false ? 1 : 0;
    return (
      <View
        style={[
          styles.weatherItem,
          {borderBottomWidth: border, borderColor: colors.lightGray},
        ]}>
        {ScrollHorizontal ? (
          <>
            <Text style={styles.time}>
              {timePart === '00:00:00'
                ? FormatedConvert(item.dt_txt)
                : TimeConvert(item.dt_txt)}
            </Text>
            <Image style={styles.imagesCon} source={{uri: imageIcons}} />
            <Text style={styles.temperature}>
              {ConvertCel(item.main.temp)}°C
            </Text>
          </>
        ) : (
          <TouchableHighlight
            activeOpacity={0.6}
            underlayColor={colors.lightGray}
            style={styles.verticalyScroll}
            onPress={() => {
              setChanges(!changes);
              setDatas(filteredData);
              // setDatas(item);
            }}>
            <View style={styles.conVer}>
              <View>
                <Text style={styles.textVer}>
                  {FormatedConvert2(item.dt_txt)}
                </Text>
              </View>
              <View style={styles.conVer}>
                <Text style={styles.textVer}>
                  {ConvertCel(item.main.feels_like)} /{' '}
                  {ConvertCel(item.main.temp_min)} °C
                </Text>
                <Image style={styles.imagesCon2} source={{uri: imageIcons}} />
                <ChevronIcon
                  name="chevron-right"
                  size={20}
                  color={colors.lightGray}
                />
              </View>
            </View>
          </TouchableHighlight>
        )}
      </View>
    );
  };

  return (
    <>
      {!changes ? (
        <FlatList
          data={dataChange}
          horizontal={ScrollHorizontal}
          renderItem={renderWeatherItem}
          showsHorizontalScrollIndicator={false}
          keyExtractor={item => item.dt.toString()}
        />
      ) : (
        <View>
          <Modals
            data={datas}
            onpress={() => {
              setChanges(!changes);
            }}
          />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  weatherItem: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  time: {
    fontSize: responsiveHeight(16),
    fontWeight: '400',
    color: colors.lightGray,
  },
  temperature: {
    fontSize: responsiveHeight(18),
    color: colors.white,
    fontStyle: 'italic',
    fontWeight: '200',
  },
  imagesCon: {
    height: responsiveHeight(60),
    width: responsiveHeight(60),
    resizeMode: 'contain',
  },
  imagesCon2: {
    height: responsiveHeight(40),
    width: responsiveHeight(40),
    resizeMode: 'contain',
  },
  verticalyScroll: {
    flex: 1,
    width: '100%',
    padding: 0,
    margin: 0,
  },
  conVer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textVer: {
    color: 'white',
    fontSize: responsiveHeight(18),
    fontWeight: '400',
  },
});

export default WeatherList;
