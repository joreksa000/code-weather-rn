import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import {ConvertCel} from '../../../application/Utils/Convert';

interface Construct {
  data: any;
}

const MainComponent: React.FC<Construct> = ({data}) => {
  const configImg = data?.weather[0]?.icon;
  const ImageData = `https://openweathermap.org/img/wn/${configImg}@2x.png`;

  return (
    <View style={style.container}>
      <View style={style.Wrapps}>
        <Image style={style.imageCon} source={{uri: ImageData}} />
        <View style={style.containerText}>
          <Text style={style.MainText}>{data?.weather[0]?.main}</Text>
          <Text style={style.DecText}>{data?.weather[0]?.description}</Text>
        </View>
      </View>
      <Text style={style.Cells}>{ConvertCel(data?.main?.temp)}°C</Text>
      <Text style={style.DecText}>
        Feels like {ConvertCel(data?.main?.feels_like)}°C
      </Text>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  containerText: {
    flexDirection: 'column',
  },
  Wrapps: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  MainText: {
    color: colors.white,
    fontWeight: '300',
    fontSize: responsiveHeight(22),
  },
  DecText: {
    color: colors.lightGray,
    fontSize: responsiveHeight(16),
    fontWeight: '300',
  },
  Cells: {
    color: colors.white,
    fontSize: responsiveHeight(70),
    fontWeight: '200',
  },
  imageCon: {
    height: responsiveHeight(60),
    width: responsiveHeight(60),
    resizeMode: 'contain',
  },
});

export default MainComponent;
