import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
} from 'react-native';
import React, {useState} from 'react';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import OptionsIcons from 'react-native-vector-icons/Ionicons';
import {ConvertCel} from '../../../application/Utils/Convert';

interface forMocals {
  onpress: () => void;
  data: any;
}
interface Buttons {
  day: any;
  date: any;
  isSelected: any;
  onpress: () => void;
}
type AbbreviatedWeekdays = {
  [key: string]: string;
};
const WeekdayButton: React.FC<Buttons> = ({day, date, onpress, isSelected}) => {
  return (
    <TouchableOpacity
      onPress={onpress}
      style={[
        style.containerButton,
        {
          backgroundColor: isSelected ? colors.lightGray : 'transparent',
        },
      ]}>
      <Text style={[style.buttonText, {color: '#929294'}]}>{day}</Text>
      <Text style={style.buttonText}>{date}</Text>
    </TouchableOpacity>
  );
};
const Modals: React.FC<forMocals> = ({onpress, data}) => {
  const [selectedDayIndex, setSelectedDayIndex] = useState(Number);

  const renderWeatherDetails = (index: any) => {
    const weather = data[index];
    const configImg = weather?.weather[0]?.icon;
    const ImageData = `https://openweathermap.org/img/wn/${configImg}@2x.png`;
    return (
      <View>
        <View style={style.coninti}>
          <View>
            <Text style={style.MainText}>{weather?.weather[0]?.main}</Text>
            <Text style={style.DecText}>
              {weather?.weather[0]?.description}
            </Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>
              {ConvertCel(weather?.main.feels_like)} /{' '}
              {ConvertCel(weather?.main.temp_min)} °C
            </Text>
            <Image style={style.imageCon} source={{uri: ImageData}} />
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Precipitation</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>{weather.wind.speed}mm</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Probability of precipitation</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>{weather.clouds.all}%</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Pressure</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>{weather.main.pressure}hPa</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Humidity</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>{weather.main.humidity}%</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>UV index</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>{weather.wind.gust}</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Sunrise</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>05:56</Text>
          </View>
        </View>
        <View style={style.coninti2}>
          <View>
            <Text style={style.MainText}>Sunset</Text>
          </View>
          <View style={style.con1}>
            <Text style={style.textVer}>18:00</Text>
          </View>
        </View>
      </View>
    );
  };

  const abbreviatedWeekdays: AbbreviatedWeekdays = {
    Monday: 'Mon',
    Tuesday: 'Tue',
    Wednesday: 'Wed',
    Thursday: 'Thu',
    Friday: 'Fri',
    Saturday: 'Sat',
    Sunday: 'Sun',
  };

  const getAbbreviatedWeekday = (weekday: string): string => {
    return abbreviatedWeekdays[weekday];
  };

  const getWeekdayFromDate = (dateString: string): string => {
    const date = new Date(dateString);
    const options = {weekday: 'long'};
    return getAbbreviatedWeekday(
      new Intl.DateTimeFormat('en-US', options).format(date),
    );
  };

  const getDayFromDate = (dateString: any) => {
    const date = new Date(dateString);
    return date.getDate();
  };
  return (
    <View
      style={{
        flex: 1,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: responsiveHeight(10),
          borderBottomWidth: 1,
          borderBottomColor: colors.white,
        }}>
        <ScrollView horizontal={true}>
          {data?.map((dayData: any, index: any) => (
            <WeekdayButton
              key={index}
              date={getDayFromDate(dayData.dt_txt)}
              day={getWeekdayFromDate(dayData.dt_txt)}
              onpress={() => setSelectedDayIndex(index)}
              isSelected={selectedDayIndex === index}
            />
          ))}
        </ScrollView>
        <TouchableOpacity
          style={{marginHorizontal: responsiveHeight(10)}}
          onPress={onpress}>
          <OptionsIcons name="options-outline" size={20} color={colors.white} />
        </TouchableOpacity>
      </View>

      <View style={{flex: 1}}>
        {selectedDayIndex !== null && renderWeatherDetails(selectedDayIndex)}
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  containerButton: {
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 10,
    height: responsiveHeight(55),
    width: responsiveHeight(50),
    marginBottom: responsiveHeight(5),
  },
  buttonText: {
    color: colors.white,
    fontSize: responsiveHeight(18),
    fontWeight: '300',
  },
  imageCon: {
    height: responsiveHeight(60),
    width: responsiveHeight(60),
    resizeMode: 'contain',
  },
  textVer: {
    color: 'white',
    fontSize: responsiveHeight(18),
    fontWeight: '400',
  },
  con1: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  MainText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: responsiveHeight(18),
  },
  DecText: {
    color: '#929294',
    fontSize: responsiveHeight(14),
    fontWeight: '300',
  },
  coninti: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: responsiveHeight(10),
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
  },
  coninti2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: responsiveHeight(10),
    height: responsiveHeight(60),
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
  },
});

export default Modals;
