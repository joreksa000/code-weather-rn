import React from 'react';
import {View, Modal, ActivityIndicator, StyleSheet} from 'react-native';
import Proptypes from 'prop-types';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';

const Loading = ({isLoading, onRequestClose}: any) => {
  return (
    <Modal
      visible={isLoading}
      transparent={true}
      onRequestClose={onRequestClose}>
      <View style={style.container}>
        <View style={style.conLoad}>
          <ActivityIndicator size={50} color={'#004CE8'} animating={true} />
        </View>
      </View>
    </Modal>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
  },
  conLoad: {
    flexDirection: 'row',
    padding: responsiveHeight(20),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 10,
  },
});

Loading.propTypes = {
  isLoading: Proptypes.bool.isRequired,
  onRequestClose: Proptypes.func,
};

export default Loading;
