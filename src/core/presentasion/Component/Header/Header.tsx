import {View, Text, StyleSheet, Pressable} from 'react-native';
import React from 'react';
import {colors} from '../../Assets/Colors/Colors';
import {responsiveHeight} from '../../../application/Utils/ResponsiveUI';
import SearchIcons from 'react-native-vector-icons/AntDesign';
import LocationIcons from 'react-native-vector-icons/Octicons';
import OptionsIcons from 'react-native-vector-icons/Ionicons';

interface Head {
  data: any;
  onpress: () => void;
}
const Header: React.FC<Head> = ({data, onpress}) => {
  return (
    <View style={style.container}>
      <Pressable style={style.buttonContainer} onPress={onpress}>
        <SearchIcons size={25} name="search1" color={colors.white} />
        <View style={style.textContainer}>
          <Text style={style.textColor}>{data?.name},</Text>
          <Text style={style.textColor}> {data?.sys?.country}</Text>
        </View>
        <LocationIcons size={15} name="location" color={colors.white} />
      </Pressable>
      <Pressable
        onPress={() => {
          console.log('setting');
        }}>
        <OptionsIcons name="options-outline" size={25} color={colors.white} />
      </Pressable>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: colors.black,
    padding: responsiveHeight(20),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: responsiveHeight(10),
    marginRight: responsiveHeight(10),
  },
  textColor: {
    color: colors.white,
    fontSize: responsiveHeight(22),
    fontWeight: '600',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
});

export default Header;
