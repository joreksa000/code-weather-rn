import React from 'react';
import {Provider} from 'react-redux';
import StoreRedux from './src/core/application/redux/Store';
import Route from './src/core/application/Route/Route';

const App = () => {
  return (
    <Provider store={StoreRedux}>
      <Route />
    </Provider>
  );
};

export default App;
